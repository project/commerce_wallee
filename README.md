# COMMERCE WALLEE

## Table of contents
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

## INTRODUCTION
 * This module integrates the PHP SDK from Wallee. You can use it as Payment Gateway on Drupal Commerce.

## REQUIREMENTS
 * You need to install the module with composer, we have a dependency on Walle PHP SDK set

## Recommended modules

## INSTALLATION
 * Install the module with composer

## CONFIGURATION
 First you have to do stuff in wallee, then drupal, and in wallee again -> see below

 * Wallee
   * You need to create a Wallee Account
   * On Wallee Backend create a new space and add there the PHP SDK
     For this, go to Settings -> Manage integrations -> Scroll down to SDK. Enable the PHP SDK
     Click on Details, and copy the Space ID, User ID and Secret and generate a new key
   * You will need Space ID, User ID, Secret for configuration in Drupal Commerce

 * Drupal
   * Go to commerce payment gateway
   * Add a new one and select the plugin "Wallee (Redirect to wallee)". 
     We recommend to setup for each payment method a own gateway.
   * You can now configure the Plugin
     * After first save, you can set additional settings (like Payment Types etc.) (SAVE!!!)
   * Please set the now the Terms checkout pane, is necessary for Wallee release
   * Copy the webhook url (this is for later line 45)

 * Wallee
   * After setup the Gateway, please setup the Webhooks like described on the plugin configuration page
   * Go to Settings -> General -> Webhook Urls's -> Create Webhook Url
   * Paste the webhook Url from Drupal (line 40)
   * Go to Settings -> General -> Webhook Listener

    1. 
        * Add new Listener. Add entity "Token" , name it "Token", 
        * Select your predefined Webhook Url.
        * Select all Entity states (Active, Deleted, Deleting, Inactive)

    1. 
        * Add new Listener. Add entity "TokenVersion" , name it "TokenVersion",
        * select your predefined Webhook Url.
        * Select all Entity states (Active, Uninitialized, Obsolete)   

    1. 
        * Add new Listener. Add entity "Transaction" , name it "Transaction",
        * select your predefined Webhook Url.
        * Select all Entity states (Completed, Failed)

## TROUBLESHOOTING

## MAINTAINERS
Current maintainers:
  * Ursin Cola (cola) https://www.drupal.org/u/cola

This project has been sponsored by:
 * soul.media
   Drupal agency, www.soul.media
