<?php

/**
 * @file
 * Hook documentation for Commerce Wallee.
 */

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\profile\Entity\ProfileInterface;
use Wallee\Sdk\Model\AddressCreate;

/**
 * Allows to alter the created address from the given profile and order.
 *
 * @param \Wallee\Sdk\Model\AddressCreate $address
 *   The Wallee address object.
 * @param \Drupal\profile\Entity\ProfileInterface $profile
 *   The profile.
 * @param \Drupal\commerce_order\Entity\OrderInterface $order
 *   The order.
 */
function hook_commerce_wallee_address_alter(AddressCreate $address, ProfileInterface $profile, OrderInterface $order) {
  $address->setSalutation($profile->get('field_salutation')->value);
}
