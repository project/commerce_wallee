<?php

namespace Drupal\commerce_wallee\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_payment\Plugin\Commerce\CheckoutPane\PaymentInformation as BasePaymentInformation;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a custom payment information pane.
 */
class PaymentInformation extends BasePaymentInformation {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form = parent::buildPaneForm($pane_form, $form_state, $complete_form);

    // Implemented like described https://docs.drupalcommerce.org/commerce2/developer-guide/checkout/replacing-existing-checkout-pane
    // Get payment information value only for authenticated users.
    if (\Drupal::currentUser()->id() == 0) {
      return $pane_form;
    }

    // Get payment method.
    $payment_method = '';
    if (isset($pane_form['payment_method']['#default_value'])) {
      $payment_method = $pane_form['payment_method']['#default_value'];
    }

    $payment_information = $form_state->getValue('payment_information');
    if (isset($payment_information['payment_method'])) {
      $payment_method = $payment_information['payment_method'];
    }

    if ($payment_method != '') {
      // Load payment method configuration.
      $payment_method = str_replace('new--credit_card--', '', $payment_method);
      $payment_gateway = \Drupal::entityTypeManager()
        ->getStorage('commerce_payment_gateway')
        ->load($payment_method);
      if ($payment_gateway instanceof PaymentGateway) {
        $plugin_configuration = $payment_gateway->getPluginConfiguration();
        if (isset($plugin_configuration['payment_reusable']) && $plugin_configuration['payment_reusable'] == 'ask') {
          // Check if user has set a value on last checkout.
          $tempstore = \Drupal::service('tempstore.private');
          $store = $tempstore->get('commerce_wallee');

          $pane_form['save_payment'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Save payment information'),
            '#description' => $this->t('Only for Visa, Master and Paypal'),
            '#default_value' => $store->get('save_payment', FALSE),
          ];
        }
      }
    }

    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form = parent::submitPaneForm($pane_form, $form_state, $complete_form);

    // Save setting on authenticated.
    if (\Drupal::currentUser()->id() != 0) {
      $values = $form_state->getValue('payment_information');
      $save_payment = FALSE;
      if (isset($values['save_payment']) and $values['save_payment'] == TRUE) {
        $save_payment = TRUE;
      }

      // Set value to tempstore.
      $tempstore = \Drupal::service('tempstore.private');
      $store = $tempstore->get('commerce_wallee');
      $store->set('save_payment', $save_payment);
    }
  }

}
