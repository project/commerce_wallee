<?php

namespace Drupal\commerce_wallee\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides terms and conditions agreement checkout pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_wallee_terms",
 *   label = @Translation("Terms agreement"),
 *   default_step = "review",
 *   wrapper_element = "fieldset",
 * )
 */
class TermsAgreementPane extends CheckoutPaneBase implements CheckoutPaneInterface {

  /**
   * The parent checkout flow.
   *
   * @var \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface
   */
  protected $checkoutFlow;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * Constructs a new TermsAgreementPane object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
   *   The parent checkout flow.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The language manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, LanguageManager $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);

    $this->checkoutFlow = $checkout_flow;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * Get default configuration.
   */
  public function defaultConfiguration() {
    return [
      'terms_url' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * Build configuration summary.
   */
  public function buildConfigurationSummary() {
    return $this->t('Terms: @terms_url', ['@terms_url' => $this->configuration['terms_url']]);
  }

  /**
   * Build configuration form.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['terms_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Terms URL'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['terms_url'],
    ];

    return $form;
  }

  /**
   * Submit configuration form.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['terms_url'] = $values['terms_url'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $terms_url = $this->configuration['terms_url'];

    if ($terms_url != '') {
      $pane_form['terms_agreement'] = [
        '#type' => 'checkbox',
        '#title' => '<a href="' . $terms_url . '" target="_blank">' . $this->t('I agree to the Terms and Conditions') . '</a>',
      ];
    }

    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $agreement = $form_state->getValue('commerce_wallee_terms');
    if (!$agreement['terms_agreement']) {
      $form_state->setError($pane_form['terms_agreement'], $this->t('You must agree to the Terms and Conditions to complete the order.'));
    }
  }

}
