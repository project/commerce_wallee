<?php

namespace Drupal\commerce_wallee\Plugin\Commerce\PaymentGateway;

use Drupal\Core\Url;
use Wallee\Sdk\Model\Token;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Wallee\Sdk\Model\EntityQuery;
use Wallee\Sdk\Service\PaymentMethodConfigurationService;
use Wallee\Sdk\ApiClient;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_price\Price;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\commerce_wallee\Services\PaymentSdk;
use Wallee\Sdk\ApiException;

/**
 * Provides the Wallee offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "wallee_redirect_checkout",
 *   label = @Translation("Wallee (Redirect to wallee)"),
 *   display_label = @Translation("Wallee"),
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_wallee\PluginForm\PaymentMethodAddForm",
 *     "offsite-payment" = "Drupal\commerce_wallee\PluginForm\RedirectCheckoutForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard", "visa",
 *   },
 * )
 */
class RedirectCheckout extends OffsitePaymentGatewayBase implements RedirectCheckoutInterface {

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private $eventDispatcher;

  /**
   * The payment sdk.
   *
   * @var \Drupal\commerce_wallee\Services\PaymentSdk
   */
  private $paymentSdk;

  /**
   * Constructs a new Wallee gateway.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin_id for the plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $paymentTypeManager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $paymentMethodTypeManager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   * @param \Drupal\commerce_wallee\Services\PaymentSdk $paymentSdk
   *   The payment sdk.
   */
  public function __construct(array $configuration,
                              $pluginId,
                              $pluginDefinition,
                              EntityTypeManagerInterface $entityTypeManager,
                              PaymentTypeManager $paymentTypeManager,
                              PaymentMethodTypeManager $paymentMethodTypeManager,
                              TimeInterface $time,
                              LoggerChannelFactoryInterface $loggerChannelFactory,
                              EventDispatcherInterface $eventDispatcher,
                              PaymentSdk $paymentSdk
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $entityTypeManager, $paymentTypeManager, $paymentMethodTypeManager, $time);
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->eventDispatcher = $eventDispatcher;
    $this->paymentSdk = $paymentSdk;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory'),
      $container->get('event_dispatcher'),
      $container->get('commerce_wallee.payment_sdk')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'space_id' => '',
      'user_id' => '',
      'secret' => '',
      'payment_configuration' => 'all',
      'payment_reusable' => 'ask',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['space_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Space ID'),
      '#description' => $this->t('This is the space id from the Wallee PHP SDK.'),
      '#default_value' => $this->configuration['space_id'],
      '#required' => TRUE,
    ];

    $form['user_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API User ID'),
      '#description' => $this->t('The user id from the Wallee PHP SDK.'),
      '#default_value' => $this->configuration['user_id'],
      '#required' => TRUE,
    ];

    $form['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Secret'),
      '#description' => $this->t('The secret from the Wallee PHP SDK.'),
      '#default_value' => $this->configuration['secret'],
      '#required' => TRUE,
    ];

    // Check if gateway is setup complete.
    if (isset($this->configuration['user_id']) and $this->configuration['user_id'] != '') {
      $methods = [];
      $methods['all'] = 'All';

      // Call Wallee API for payment methods.
      try {
        $client = new ApiClient($this->configuration['user_id'], $this->configuration['secret']);
        $configuration = new PaymentMethodConfigurationService($client);
        $query = new EntityQuery();
        $query->setLanguage('en');
        $configurations = $configuration->search($this->configuration['space_id'], $query);
        foreach ($configurations as $config) {
          $methods[$config->getId()] = $config->getName();
        }
      }
      catch (ApiException $e) {
        $this->messenger()->addError($this->t('Error on Communication with Wallee, please check your settings.'));
      }

      if (count($methods) > 0) {
        // List all payment types they are possible from Wallee.
        $form['payment_configuration'] = [
          '#type' => 'radios',
          '#options' => $methods,
          '#title' => $this->t('Payment Types'),
          '#description' => $this->t('If no selected we will use all.'),
          '#default_value' => $this->configuration['payment_configuration'],
        ];
      }

      // Allow user to save token.
      $form['payment_reusable'] = [
        '#type' => 'select',
        '#title' => $this->t('Request token and create reusable payment method'),
        '#description' => $this->t('Allows to reuse the payment information for additional payments. Wallee allows right now only Visa, Master and Paypal.'),
        '#default_value' => $this->configuration['payment_reusable'],
        '#options' => [
          'always' => $this->t('Always, payment methods are created for logged in users only'),
          'ask' => $this->t('Ask the user'),
          'never' => $this->t('Never'),
        ],
      ];

      // Webhook configuration info.
      $webhook_info = [];
      $webhook_info[] = '- Token | Name: "Token". States: "Active", "Inactive", "Deleting", "Deleted"';
      $webhook_info[] = '- Token Version | Name: "TokenVersion". States: "Active", "Uninitialized", "Obsolete"';
      $webhook_info[] = '- Transaction | Name: "Transaction". States: "Completed", "Failed"';

      $form['webhook_url_info'] = [
        '#markup' => '<b>' . $this->t('You need to setup following webhooks in PostFinance Backend') . '</b><br>' . implode('<br>', $webhook_info),
      ];

      $webhook_url = Url::fromRoute('commerce_wallee.webhook')->setAbsolute()->toString();
      $form['webhook_url'] = [
        '#markup' => '<br>' . $this->t('The webhook url is %url', ['%url' => $webhook_url]) ,
      ];
    }
    else {
      $form['payment_configuration'] = [
        '#markup' => '<b>' . $this->t('After first saving, you can edit the settings again and set the payment methods you want.') . '</b>',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['type'] = 'commerce_wallee';
    $this->configuration['space_id'] = $values['space_id'];
    $this->configuration['user_id'] = $values['user_id'];
    $this->configuration['secret'] = $values['secret'];
    $this->configuration['payment_reusable'] = ($values['payment_reusable'] ?? FALSE);
    $this->configuration['payment_configuration'] = ($values['payment_configuration'] ?? 'all');
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    parent::onReturn($order, $request);

    $paymentStorage = $this->entityTypeManager->getStorage('commerce_payment');
    $payments = $paymentStorage->loadMultipleByOrder($order);

    $success = FALSE;
    foreach ($payments as $payment) {
      // Check if gateway is paymentgateway entity.
      if ($payment->payment_gateway->entity instanceof PaymentGateway) {
        $configuration = $payment->getPaymentGateway()->getPluginConfiguration();
        if (isset($configuration['type']) and $configuration['type'] == 'commerce_wallee') {
          $transaction_id = $payment->getRemoteId();
          $transation = $this->paymentSdk->readTransaction($configuration['user_id'], $configuration['secret'], $configuration['space_id'], $transaction_id);

          // Check state of transaction.
          // More infos https://app-wallee.com/de-ch/doc/payment/transaction-process
          if (isset($transation['state'])) {
            // Transform wallee state.
            $state = $this->paymentSdk->transformTransactionState($transation['state']);
            if ($state != NULL and $state != 'void') {
              $success = TRUE;
            }

            // Update payment.
            if ($state != NULL) {
              $payment->set('state', $state);
              $payment->set('remote_state', $state);
              $payment->save();
            }
          }

          // Check token on transaction.
          if (isset($transation['token']) and $transation['token'] instanceof Token) {
            // Anonymous cannot have tokens.
            if ($payment->getOrder()->uid->target_id != 0) {
              $remote_id = $transation['token']->getId();
              $user_id = $payment->getOrder()->uid->target_id;
              $billing_profile = $order->getBillingProfile();
              $this->paymentSdk->createPaymentMethod($payment->getPaymentGateway(), $remote_id, $user_id, $billing_profile);
            }
          }
        }
      }
    }

    if ($success == FALSE) {
      // Error on payment.
      throw new PaymentGatewayException('Payment incomplete or declined');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    parent::onCancel($order, $request);
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    parent::onNotify($request);
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);

    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();

    // Validate the requested amount.
    $this->assertRefundAmount($payment, $amount);

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->state = 'partially_refunded';
    }
    else {
      $payment->state = 'refunded';
    }

    // Check if gateway is paymentgateway entity.
    if ($payment->payment_gateway->entity instanceof PaymentGateway) {
      // Call remote refund on Wallee.
      $configuration = $payment->getPaymentGateway()->getPluginConfiguration();
      $refund = $this->paymentSdk->createRefund($configuration['user_id'], $configuration['secret'], $configuration['space_id'], $amount->getNumber(), $payment->getRemoteId());
      if ($refund->getState() == 'SUCCESSFUL') {
        // Save refund.
        $payment->setRefundedAmount($new_refunded_amount);
        $payment->save();

        $this->messenger()->addStatus($this->t('Refund on payment %payment', [
          '%payment' => $payment->id(),
        ]));
      }
      else {
        // Error on refund.
        $this->messenger()->addError($this->t('Error refund on payment %payment', [
          '%payment' => $payment->id(),
        ]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    // Check if gateway is paymentgateway entity.
    if ($payment->payment_gateway->entity instanceof PaymentGateway) {
      // Get plugin configuration.
      $configuration = $payment->payment_gateway->entity->getPluginConfiguration();

      // Payment data.
      $currency = $payment->getAmount()->getCurrencyCode();
      $amount = $payment->getAmount()->getNumber();
      $order = $payment->getOrder();
      $profiles = $order->collectProfiles();

      // Create addresses.
      $billingAddress = NULL;
      if (isset($profiles['billing'])) {
        $billingAddress = $this->paymentSdk->createAddress($profiles['billing'], $order);
      }

      // Create addresses.
      $shippingAddress = NULL;
      if (isset($profiles['shipping'])) {
        $shippingAddress = $this->paymentSdk->createAddress($profiles['shipping'], $order);
      }
      $token = $payment->payment_method->entity->remote_id->value;

      // Set environment.
      if ($configuration['mode'] == 'test') {
        $environment = 'PREVIEW';
      }
      else {
        $environment = 'LIVE';
      }

      // Create transaction with no interaction.
      $transaction = $this->paymentSdk->createTransactionNoUserInteraction($configuration['user_id'], $configuration['secret'], $configuration['space_id'], $currency, $order, $amount, $billingAddress, $shippingAddress, $token, $environment);

      // Error handling for transaction.
      if (!isset($transaction['id']) or $transaction['id'] == '') {
        $this->messenger()->addError($this->t('Error on payment gateway, no transaction found.'));
        throw new PaymentGatewayException('Error on payment gateway, no transaction found.');
      }
      if (!isset($transaction['state']) or $transaction['state'] == '') {
        $this->messenger()->addError($this->t('Error on payment gateway, no transaction found.'));
        throw new PaymentGatewayException('Error on payment gateway, no transaction found.');
      }
      if ($transaction['state'] == 'FAILED') {
        $message = 'Error on payment gateway, failure on transaction.';

        // Check if token is disabled.
        if (isset($transaction['token'])) {
          if ($transaction['token']->getState() == 'INACTIVE') {
            $message = 'Error on payment gateway, payment token is inactive.';
          }
        }

        $this->messenger()->addError($this->t($message));
        throw new PaymentGatewayException($message);
      }

      // Check state of transaction.
      // More infos https://app-wallee.com/de-ch/doc/payment/transaction-process
      if (isset($transaction['state'])) {
        // Transform wallee state.
        $state = $this->paymentSdk->transformTransactionState($transaction['state']);

        // Update payment.
        if ($state != NULL) {
          $payment->set('remote_id', $transaction['id']);
          $payment->set('state', $state);
          $payment->set('remote_state', $state);
          $payment->save();
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {

  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete token on Wallee backend.
    $this->paymentSdk->deletePaymentMethod($payment_method);
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {
    // @todo not working right now on wallee.
  }

}
