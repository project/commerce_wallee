<?php

namespace Drupal\commerce_wallee\Services;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\profile\Entity\ProfileInterface;
use Wallee\Sdk\Model\AddressCreate;

/**
 * Event handler for Wallee PHP SDK.
 *
 * @package Drupal\commerce_wallee\Services
 */
interface PaymentSdkInterface {

  /**
   * Creates a Wallee address from a profile and order.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The profile.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Wallee\Sdk\Model\Address
   *   The address object.
   */
  public function createAddress(ProfileInterface $profile, OrderInterface $order);

  /**
   * Reads a Wallee transaction.
   *
   * @param string $user_id
   *   The user id.
   * @param string $secret
   *   The secret.
   * @param string $space_id
   *   The space id.
   * @param string $transaction_id
   *   The transaction id.
   *
   * @return array
   *   The transaction object.
   */
  public function readTransaction(string $user_id, string $secret, string $space_id, string $transaction_id);

  /**
   * Create transaction.
   *
   * @param string $user_id
   *   The user id.
   * @param string $secret
   *   The secret.
   * @param string $space_id
   *   The space id.
   * @param string $return_url
   *   The return url.
   * @param string $cancel_url
   *   The cancel url.
   * @param string $currency
   *   The currency.
   * @param string $order_id
   *   The order id.
   * @param string $amount
   *   The amount.
   * @param \Wallee\Sdk\Model\AddressCreate|null $billingAddress
   *   The billing address.
   * @param \Wallee\Sdk\Model\AddressCreate|null $shippingAddress
   *   The shipping address.
   * @param string $environment
   *   The environment.
   * @param array $pluginConfiguration
   *   The plugin configuration.
   *
   * @return mixed
   *   The transaction object.
   */
  public function createTransaction(string $user_id, string $secret, string $space_id, string $return_url, string $cancel_url, string $currency, $order_id, $amount, ?AddressCreate $billingAddress, ?AddressCreate $shippingAddress, string $environment = 'PREVIEW', array $pluginConfiguration = []);

  /**
   * Creates a refund.
   *
   * @param string $user_id
   *   The user id.
   * @param string $secret
   *   The secret.
   * @param string $space_id
   *   The space id.
   * @param string $amount
   *   The amount.
   * @param string $remote_id
   *   The remote id.
   *
   * @return mixed
   *   The refund object.
   */
  public function createRefund(string $user_id, string $secret, string $space_id, string $amount, string $remote_id);

  /**
   * Get the token version.
   *
   * @param int $token_version_id
   *   The Token version id.
   *
   * @return \Wallee\Sdk\Model\TokenVersion
   *   The token version object.
   */
  public function getTokenVersion(PaymentGatewayInterface $paymentGateway, $token_version_id);

}
