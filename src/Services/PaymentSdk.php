<?php

namespace Drupal\commerce_wallee\Services;

use Email\Parse;
use Psr\Log\LoggerInterface;
use Wallee\Sdk\Model\Token;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Wallee\Sdk\Model\TransactionPending;
use Wallee\Sdk\Service\TokenService;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Wallee\Sdk\Model\TokenVersion;
use Wallee\Sdk\Service\TokenVersionService;
use Drupal\user\Entity\User;
use Wallee\Sdk\Model\Refund;
use Wallee\Sdk\Model\Transaction;
use Wallee\Sdk\Service\TransactionService;
use Wallee\Sdk\Model\TokenizationMode;
use Wallee\Sdk\Model\TransactionCreate;
use Wallee\Sdk\Model\LineItemType;
use Wallee\Sdk\Model\LineItemCreate;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Wallee\Sdk\ApiClient;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\profile\Entity\ProfileInterface;
use Wallee\Sdk\Model\AddressCreate;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Session\AccountProxy;

/**
 * Event handler for Wallee PHP SDK.
 */
class PaymentSdk implements PaymentSdkInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The private tempstore service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $privateTempStore;

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * PaymentSdk constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $privateTempStore
   *   The private tempstore service.
   * @param \Drupal\Core\Session\AccountProxy $currentUser
   *   The current user account.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, PrivateTempStoreFactory $privateTempStore, AccountProxy $currentUser, LoggerInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->privateTempStore = $privateTempStore;
    $this->currentUser = $currentUser;
    $this->logger = $logger;
  }

  /**
   * Create Wallee PHP SDK Client.
   *
   * @param string $user_id
   *   The user id.
   * @param string $secret
   *   The secret.
   *
   * @return \Wallee\Sdk\ApiClient
   *   The client.
   */
  private function apiClient(string $user_id, string $secret) {
    return new ApiClient($user_id, $secret);
  }

  /**
   * Create Wallee line item.
   *
   * @param string $order_id
   *   The order id.
   * @param string $amount
   *   The amount.
   *
   * @return \Wallee\Sdk\Model\LineItemCreate
   *   The line item.
   */
  private function createLineItem(string $order_id, string $amount) {
    $lineItem = new LineItemCreate();
    $lineItem->setName('Order ' . $order_id);
    $lineItem->setUniqueId('orderid-' . $order_id);
    $lineItem->setQuantity(1);
    $lineItem->setAmountIncludingTax($amount);
    $lineItem->setType(LineItemType::FEE);

    return $lineItem;
  }

  /**
   * {@inheritdoc}
   */
  public function createAddress(ProfileInterface $profile, OrderInterface $order) {

    $walleeAddress = new AddressCreate();

    /** @var \Drupal\address\AddressInterface $address */
    $address = $profile->hasField('address') ? $profile->get('address')->first() : NULL;
    if ($address) {
      $walleeAddress->setOrganizationName($address->getOrganization());
      $walleeAddress->setFamilyName($address->getFamilyName());
      $walleeAddress->setGivenName($address->getGivenName());
      $walleeAddress->setStreet($address->getAddressLine1());
      $walleeAddress->setPostCode($address->getPostalCode());
      $walleeAddress->setCity($address->getLocality());
      $walleeAddress->setCountry($address->getCountryCode());
    }
    $walleeAddress->setEmailAddress($order->getEmail());

    \Drupal::moduleHandler()->alter('commerce_wallee_address', $walleeAddress, $profile, $order);

    return $walleeAddress;
  }

  /**
   * Create Wallee transaction.
   *
   * @param string $user_id
   *   The user id.
   * @param string $secret
   *   The secret.
   * @param string $space_id
   *   The space id.
   * @param string $return_url
   *   The return url.
   * @param string $cancel_url
   *   The cancel url.
   * @param string $currency
   *   The currency.
   * @param string $order_id
   *   The order id.
   * @param string $amount
   *   The amount.
   * @param Wallee\Sdk\Model\AddressCreate|null $billingAddress
   *   The billing address.
   * @param Wallee\Sdk\Model\AddressCreate|null $shippingAddress
   *   The shipping address.
   * @param string $environment
   *   The environment.
   * @param array $pluginConfiguration
   *   The plugin configuration.
   *
   * @return array
   *   The transaction.
   *
   * @throws \Wallee\Sdk\ApiException
   * @throws \Wallee\Sdk\Http\ConnectionException
   * @throws \Wallee\Sdk\VersioningException
   */
  public function createTransaction(string $user_id, string $secret, string $space_id, string $return_url, string $cancel_url, string $currency, $order_id, $amount, ?AddressCreate $billingAddress, ?AddressCreate $shippingAddress, string $environment = 'PREVIEW', array $pluginConfiguration = []) {
    // Set user language.
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $language_string = strtolower($language) . '-' . strtoupper($language);

    $transactionPayload = new TransactionCreate();
    $transactionPayload->setMerchantReference($order_id);
    $transactionPayload->setLanguage($language_string);
    $transactionPayload->setSuccessUrl($return_url);
    $transactionPayload->setFailedUrl($cancel_url);
    $transactionPayload->setEnvironment($environment);
    if ($billingAddress) {
      $transactionPayload->setBillingAddress($billingAddress);
    }
    if ($shippingAddress) {
      $transactionPayload->setShippingAddress($shippingAddress);
    }
    $transactionPayload->setCurrency($currency);
    $transactionPayload->setLineItems([$this->createLineItem($order_id, $amount)]);
    $transactionPayload->setAutoConfirmationEnabled(TRUE);

    if (isset($pluginConfiguration['payment_configuration']) and $pluginConfiguration['payment_configuration'] != 'all') {
      $transactionPayload->setAllowedPaymentMethodConfigurations($pluginConfiguration['payment_configuration']);
    }

    if ($this->shouldRequestToken($pluginConfiguration)) {
      // Force token generation on wallee.
      $transactionPayload->setTokenizationMode(TokenizationMode::FORCE_CREATION_WITH_ONE_CLICK_PAYMENT);
      $transactionPayload->setCustomerEmailAddress($transactionPayload->getBillingAddress()->getEmailAddress());
      if ($this->currentUser->id() != 0) {
        $transactionPayload->setCustomerId($this->currentUser->id());
      }
    }

    // Allow alter transaction over hook.
    \Drupal::moduleHandler()->invokeAll('commerce_wallee_alter_transaction',
      [
        &$transactionPayload,
        $order_id,
      ]);

    // Create transaction.
    $client = $this->apiClient($user_id, $secret);
    $transaction = $client->getTransactionService()->create($space_id, $transactionPayload);

    return [
      'transaction_id' => $transaction->getId(),
      'redirect_url' => $client->getTransactionPaymentPageService()->paymentPageUrl($space_id, $transaction->getId()),
    ];
  }

  /**
   * Create Wallee transaction with no user interaction.
   *
   * @param string $user_id
   *   The user id.
   * @param string $secret
   *   The secret.
   * @param string $space_id
   *   The space id.
   * @param string $currency
   *   The currency.
   * @param Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param string $amount
   *   The amount.
   * @param Wallee\Sdk\Model\AddressCreate|null $billingAddress
   *   The billing address.
   * @param Wallee\Sdk\Model\AddressCreate|null $shippingAddress
   *   The shipping address.
   * @param string $token
   *   The token.
   * @param string $environment
   *   The environment.
   *
   * @return array
   *   The transaction id and the redirect url.
   *
   * @throws \Wallee\Sdk\ApiException
   * @throws \Wallee\Sdk\Http\ConnectionException
   * @throws \Wallee\Sdk\VersioningException
   */
  public function createTransactionNoUserInteraction($user_id, $secret, $space_id, $currency, OrderInterface $order, $amount, ?AddressCreate $billingAddress, ?AddressCreate $shippingAddress, $token, $environment = 'PREVIEW') {

    $transactionPayload = new TransactionCreate();
    $transactionPayload->setMerchantReference($order->id());
    $transactionPayload->setEnvironment($environment);
    if ($billingAddress) {
      $transactionPayload->setBillingAddress($billingAddress);
    }
    if ($shippingAddress) {
      $transactionPayload->setShippingAddress($shippingAddress);
    }
    $transactionPayload->setCurrency($currency);
    $transactionPayload->setLineItems([$this->createLineItem($order->id(), $amount)]);
    $transactionPayload->setAutoConfirmationEnabled(TRUE);

    // Set token to transaction.
    $transactionPayload->setToken($token);

    try {
      // Create transaction.
      $client = $this->apiClient($user_id, $secret);
      $transaction = $client->getTransactionService()->create($space_id, $transactionPayload);
      $transaction_service = new TransactionService($client);
      $transaction_return = $transaction_service->processWithoutUserInteraction($space_id, $transaction->getId());
    }
    catch (\Exception $e) {
      throw  new PaymentGatewayException($e->getMessage());
    }

    return [
      'id' => $transaction_return->getId(),
      'authorization_amount' => $transaction_return->getAuthorizationAmount(),
      'authorization_environment' => $transaction_return->getAuthorizationEnvironment(),
      'authorized_on' => $transaction_return->getAuthorizedOn(),
      'completed_amount' => $transaction_return->getCompletedAmount(),
      'completed_on' => $transaction_return->getCompletedOn(),
      'currency' => $transaction_return->getCurrency(),
      'state' => $transaction_return->getState(),
      'token' => $transaction_return->getToken(),
    ];
  }

  /**
   * Transform Wallee transaction state to drupal commerce state.
   *
   * @param string $wallee_state
   *   Wallee state.
   *
   * @return string|null
   *   Commerce state.
   */
  public function transformTransactionState(string $wallee_state) {
    // State is authorized.
    $commerce_sate = NULL;
    if ($wallee_state == 'VOIDED') {
      $commerce_sate = 'void';
    }
    if ($wallee_state == 'AUTHORIZED') {
      $commerce_sate = 'authorize';
    }
    if ($wallee_state == 'COMPLETED') {
      $commerce_sate = 'completed';
    }
    if ($wallee_state == 'FULFILL') {
      $commerce_sate = 'completed';
    }

    return $commerce_sate;
  }

  /**
   * Read transaction.
   *
   * @param string $user_id
   *   User id.
   * @param string $secret
   *   Secret.
   * @param string $space_id
   *   Space id.
   * @param string $transaction_id
   *   Transaction id.
   *
   * @return array
   *   Transaction data.
   *
   * @throws \Wallee\Sdk\ApiException
   * @throws \Wallee\Sdk\Http\ConnectionException
   * @throws \Wallee\Sdk\VersioningException
   */
  public function readTransaction(string $user_id, string $secret, string $space_id, string $transaction_id) {
    $client = $this->apiClient($user_id, $secret);

    $transaction = $client->getTransactionService()->read($space_id, $transaction_id);

    return [
      'id' => $transaction_id,
      'authorization_amount' => $transaction->getAuthorizationAmount(),
      'authorization_environment' => $transaction->getAuthorizationEnvironment(),
      'authorized_on' => $transaction->getAuthorizedOn(),
      'completed_amount' => $transaction->getCompletedAmount(),
      'completed_on' => $transaction->getCompletedOn(),
      'currency' => $transaction->getCurrency(),
      'state' => $transaction->getState(),
      'token' => $transaction->getToken(),
    ];
  }

  /**
   * Create refund for transaction.
   */
  public function createRefund($user_id, $secret, $space_id, $amount, $remote_id) {
    // Create refund object.
    $trans = new Transaction();
    $trans->setId($remote_id);

    $refund = new Refund();
    $refund->setAmount($amount);
    $refund->setExternalId(time());
    $refund->setMerchantReference('commerce_wallee');
    $refund->setTransaction($trans);
    $refund->setType('MERCHANT_INITIATED_ONLINE');

    // Setup API client and call refund.
    $client = $this->apiClient($user_id, $secret);
    return $client->getRefundService()->refund($space_id, $refund);
  }

  /**
   * Create Payment method with user data.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $commerce_payment_gateway
   *   Commerce payment gateway.
   * @param string $remote_id
   *   Remote id.
   * @param string $user_id
   *   User id.
   * @param \Drupal\profile\Entity\ProfileInterface $billing_profile
   *   Billing profile.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\Core\Entity\EntityInterface[]|false|null
   *   Payment method.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Wallee\Sdk\ApiException
   * @throws \Wallee\Sdk\Http\ConnectionException
   * @throws \Wallee\Sdk\VersioningException
   */
  public function createPaymentMethod(PaymentGatewayInterface $commerce_payment_gateway, string $remote_id, string $user_id, ProfileInterface $billing_profile) {
    // Owner.
    $owner = User::load($user_id);
    $owner_timezone = $owner->getTimeZone();

    // Set a default timezone, if user don't have one.
    if ($owner_timezone == NULL) {
      $owner_timezone = 'UTC';
    }

    // Get detail information from token version.
    $configuration = $commerce_payment_gateway->getPluginConfiguration();
    $client = $this->apiClient($configuration['user_id'], $configuration['secret']);
    $tokenVersionService = new TokenVersionService($client);
    $version = $tokenVersionService->activeVersion($configuration['space_id'], $remote_id);
    if ($version instanceof TokenVersion) {
      $card_endingnumber = $version->getName();
      $expires_timestamp = NULL;
      if ($version->getExpiresOn() instanceof \DateTime) {
        $expires_timestamp = $version->getExpiresOn()->setTimezone(new \DateTimeZone($owner_timezone));
        $expires_timestamp = $expires_timestamp->getTimestamp();
      }

      // Transform credit card type.
      $card_type = $this->mapCardType($version->getPaymentConnectorConfiguration()->getName());
      if ($card_type == FALSE) {
        // Set a default value.
        $card_type = 'visa';
      }

      // Check if remote id exists.
      $payment_methods = $this->entityTypeManager->getStorage('commerce_payment_method')->loadByProperties([
        'remote_id' => $remote_id,
        'payment_gateway' => $commerce_payment_gateway->id(),
      ]);
      $payment_method = NULL;
      if (count($payment_methods) > 0) {
        // Update existing.
        $payment_method = current($payment_methods);
        switch ($card_type) {
          case 'visa':
          case 'mastercard':
            $payment_method->expires->value = $expires_timestamp;
            $payment_method->card_type->value = $card_type;
            $payment_method->card_number->value = $card_endingnumber;
            $payment_method->card_exp_month->value = date('m', $expires_timestamp);
            $payment_method->card_exp_year->value = date('y', $expires_timestamp);
            break;
        }
      }
      else {
        // Create new one.
        $values = [];
        switch ($card_type) {
          case 'visa':
          case 'mastercard':
            $values = [
              'type' => 'credit_card',
              'payment_gateway' => $commerce_payment_gateway,
              'remote_id' => $remote_id,
              'uid' => $user_id,
              'billing_profile' => $billing_profile,
              'reusable' => TRUE,
              'is_default' => TRUE,
              'expires' => $expires_timestamp,
              'card_type' => $card_type,
              'card_number' => $card_endingnumber,
              'card_exp_month' => date('m', $expires_timestamp),
              'card_exp_year' => date('y', $expires_timestamp),
            ];
            break;

          case 'paypal':
            // Extract mail address.
            $mail_address = explode(' (', $card_endingnumber)[0] ?? $card_endingnumber;

            $values = [
              'type' => 'paypal',
              'payment_gateway' => $commerce_payment_gateway,
              'remote_id' => $remote_id,
              'uid' => $user_id,
              'billing_profile' => $billing_profile,
              'reusable' => TRUE,
              'is_default' => TRUE,
              'paypal_mail' => $mail_address,
            ];
            break;
        }

        if ($values) {
          $payment_method = $this->entityTypeManager->getStorage('commerce_payment_method')->create($values);
        }
      }
      if ($payment_method) {
        $payment_method->save();
      }
      return $payment_method;
    }
    else {
      return NULL;
    }
  }

  /**
   * Delete Payment method on wallee.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   *
   * @throws \Wallee\Sdk\ApiException
   * @throws \Wallee\Sdk\Http\ConnectionException
   * @throws \Wallee\Sdk\VersioningException
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Check if gateway is paymentgateway entity.
    if ($payment_method->payment_gateway->entity instanceof PaymentGateway) {
      $configuration = $payment_method->payment_gateway->entity->getPluginConfiguration();
      $client = $this->apiClient($configuration['user_id'], $configuration['secret']);
      return $client->getTokenService()->delete($configuration['space_id'], $payment_method->getRemoteId());
    }
    return FALSE;
  }

  /**
   * Update Payment method on wallee.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   * @param string $status
   *   The status.
   *
   * @throws \Wallee\Sdk\ApiException
   * @throws \Wallee\Sdk\Http\ConnectionException
   * @throws \Wallee\Sdk\VersioningException
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method, string $status = '') {
    // Extract full URL.
    $current_page = parse_url((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
    $url = $current_page['scheme'] . '://' . $current_page['host'] . $current_page['path'];

    // Check if gateway is paymentgateway entity.
    if ($payment_method->payment_gateway->entity instanceof PaymentGateway) {
      // Create API Client.
      $pluginConfiguration = $payment_method->payment_gateway->entity->getPluginConfiguration();
      $client = $this->apiClient($pluginConfiguration['user_id'], $pluginConfiguration['secret']);

      if ($status == '') {
        $tokenService = new TokenService($client);
        $transactionPayload = $tokenService->createTransactionForTokenUpdate($pluginConfiguration['space_id'], $payment_method->getRemoteId());
        $pendingTransaction = new TransactionPending();

        if (isset($pluginConfiguration['payment_configuration']) and $pluginConfiguration['payment_configuration'] != 'all') {
          $pendingTransaction->setAllowedPaymentMethodConfigurations($pluginConfiguration['payment_configuration']);
        }

        $pendingTransaction->setId($transactionPayload->getId());
        $pendingTransaction->setVersion($transactionPayload->getVersion());
        $pendingTransaction->setSuccessUrl($url . '?wallee_update=success');
        $pendingTransaction->setFailedUrl($url . '?wallee_update=failed');
        $transaction = $client->getTransactionService()->update($pluginConfiguration['space_id'], $pendingTransaction);
        $redirectionUrl = $client->getTransactionPaymentPageService()->paymentPageUrl($pluginConfiguration['space_id'], $transaction->getId());
        header('Location: ' . $redirectionUrl);
        exit();
      }

      if ($status == 'success') {
        \Drupal::messenger()->addMessage($this->t('The information will be updated soon.'));
        $response = new RedirectResponse('/user/' . $this->currentUser->id() . '/payment-methods');
        $response->send();
      }

      if ($status == 'failed') {
        \Drupal::messenger()->addError($this->t('Update failed, please try again later.'));

        $current_user = \Drupal::currentUser();
        $response = new RedirectResponse('/user/' . $current_user->id() . '/payment-methods');
        $response->send();
      }
    }
  }

  /**
   * Webhook for Token update.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $paymentGateway
   *   Payment gateway.
   * @param array $data
   *   Webhook data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Wallee\Sdk\ApiException
   * @throws \Wallee\Sdk\Http\ConnectionException
   * @throws \Wallee\Sdk\VersioningException
   */
  public function webhookToken(PaymentGatewayInterface $paymentGateway, array $data) {
    // Get configuration and create client.
    $configuration = $paymentGateway->getPluginConfiguration();
    $client = $this->apiClient($configuration['user_id'], $configuration['secret']);

    // Get token entity.
    $payment_method = $this->entityTypeManager->getStorage('commerce_payment_method')->loadByProperties(
      [
        'remote_id' => $data['entityId'],
        'payment_gateway.target_id' => $paymentGateway->id(),
      ]);
    if (count($payment_method) > 0 and current($payment_method) instanceof PaymentMethod) {
      // Get token with entity id.
      $payment_method = current($payment_method);
      $tokenService = new TokenService($client);
      $token = $tokenService->read($configuration['space_id'], $data['entityId']);
      if ($token instanceof Token) {
        // Check token state: DELETE.
        if ($token->getState() == 'DELETED') {
          $payment_method->delete();
        }
      }
    }
  }

  /**
   * Webhook for Token Version update.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGateway $paymentGateway
   *   Payment gateway.
   * @param array $data
   *   Webhook data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Wallee\Sdk\ApiException
   * @throws \Wallee\Sdk\Http\ConnectionException
   * @throws \Wallee\Sdk\VersioningException
   */
  public function webhookTokenVersion(PaymentGatewayInterface $paymentGateway, array $data, TokenVersion $version) {
    // Get token entity.
    $remote_id = $version->getToken()->getId();
    $card_endingnumber = $version->getName();

    if ($version->getPaymentConnectorConfiguration() == NULL) {
      return FALSE;
    }

    $card_type = $this->mapCardType($version->getPaymentConnectorConfiguration()->getName());
    $payment_method = $this->entityTypeManager->getStorage('commerce_payment_method')->loadByProperties(
      [
        'remote_id' => $remote_id,
        'payment_gateway.target_id' => $paymentGateway->id(),
      ]);
    if (count($payment_method) > 0 and current($payment_method) instanceof PaymentMethod) {
      // Get token with entity id.
      $payment_method = current($payment_method);
      if ($card_type == FALSE) {
        // Set a default value.
        $card_type = 'visa';
      }

      switch ($card_type) {
        case 'visa':
        case 'mastercard':
          $owner = $payment_method->uid->entity;
          $owner_timezone = $owner->getTimeZone();

          $expires_timestamp = '';
          if ($version->getExpiresOn() != '') {
              // Set a default timezone, if user of payment method don't have one.
              if ($owner_timezone == NULL) {
                $config = \Drupal::config('system.date');
                $owner_timezone = $config->get('timezone.default');
              }
            $expires_timestamp = $version->getExpiresOn()->setTimezone(new \DateTimeZone($owner_timezone));
            $expires_timestamp = $expires_timestamp->getTimestamp();
          }

          $payment_method->expires->value = $expires_timestamp;
          $payment_method->card_type->value = $card_type;
          $payment_method->card_number->value = $card_endingnumber;
          $payment_method->card_exp_month->value = date('m', $expires_timestamp);
          $payment_method->card_exp_year->value = date('y', $expires_timestamp);
          break;
      }

      // Update existing token with remote information.
      $payment_method->save();
    }
  }

  /**
   * Webhook for Transaction update.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGateway $paymentGateway
   *   The payment gateway.
   * @param array $data
   *   The webhook data.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function webhookTransaction(PaymentGatewayInterface $paymentGateway, array $data) {
    // Get configuration and create client.
    $configuration = $paymentGateway->getPluginConfiguration();

    // Get token entity.
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payments = $payment_storage->loadByProperties(
      [
        'remote_id' => $data['entityId'],
        'payment_gateway' => $paymentGateway->id(),
      ]);
    $payment = reset($payments);
    if ($payment instanceof PaymentInterface) {
      $transaction = $this->readTransaction($configuration['user_id'], $configuration['secret'], $configuration['space_id'], $data['entityId']);
      // Check state of transaction.
      // More infos https://app-wallee.com/de-ch/doc/payment/transaction-process
      if (isset($transaction['state'])) {
        $this->logger->notice('Webhook transaction state: @state', ['@state' => $transaction['state']]);
        // Transform wallee state.
        $state = $this->transformTransactionState($transaction['state']);

        // Update payment.
        if ($state != NULL) {

          // This does not need to update the order, but use the loadForUpdate()
          // method of the order storage, this locking system requires
          // https://www.drupal.org/project/commerce/issues/3043180.
          $order_storage = $this->entityTypeManager->getStorage('commerce_order');
          if (\method_exists($order_storage, 'loadForUpdate')) {
            $order_storage->loadForUpdate($payment->getOrderId());

            // Reload the payment entity.
            $payment_storage->resetCache([$payment->id()]);
            $payment = $payment_storage->load($payment->id());
          }

          $payment->state->value = $state;
          $payment->remote_state->value = $state;
          $payment->completed->value = time();
          $payment->save();

          // The order storage does not have a public method to release the
          // lock. The current implementation uss the lock backend with
          // a given lock id.
          if (\method_exists($order_storage, 'loadForUpdate')) {
            $lock_id = 'commerce_order_update:' . $payment->getOrderId();
            \Drupal::lock()->release($lock_id);
          }
        }
        else {
          $this->logger->error('State is null on transaction: @remoteid on payment_gateway: @paymentgateway',
            [
              '@remoteid' => $data['entityId'],
              '@paymentgateway' => $paymentGateway->id(),
            ]);
        }
      }
      else {
        $this->logger->error('No state on transaction: @remoteid on payment_gateway: @paymentgateway',
          [
            '@remoteid' => $data['entityId'],
            '@paymentgateway' => $paymentGateway->id(),
          ]);
      }
    }
    else {
      $this->logger->error('No payment found with remote_id: @remoteid on payment_gateway: @paymentgateway',
        [
          '@remoteid' => $data['entityId'],
          '@paymentgateway' => $paymentGateway->id(),
        ]);
    }
  }

  /**
   * Maps the credit card type to a Commerce credit card type.
   *
   * @param string $wallee_card_type
   *   The Wallee credit card type.
   *
   * @return mixed|string
   *   The Commerce credit card type.
   */
  public function mapCardType(string $wallee_card_type) {
    $card_type = '';
    if (strpos(strtolower($wallee_card_type), 'visa') !== FALSE) {
      $card_type = 'visa';
    }
    if (strpos(strtolower($wallee_card_type), 'mastercard') !== FALSE) {
      $card_type = 'mastercard';
    }
    if (strpos(strtolower($wallee_card_type), 'paypal') !== FALSE) {
      $card_type = 'paypal';
    }

    return $card_type;
  }

  /**
   * Returns whether a token should be requested.
   *
   * @param array $plugin_configuration
   *   The payment gateway plugin configuration.
   *
   * @return bool
   *   TRUE if a token should be requested.
   */
  protected function shouldRequestToken(array $plugin_configuration) {
    if ($plugin_configuration['payment_reusable'] == 'always') {
      return TRUE;
    }

    if ($plugin_configuration['payment_reusable'] == 'ask' && $this->currentUser->id() != 0) {
      // Get user temp store.
      $store = $this->privateTempStore->get('commerce_wallee');
      return $store->get('save_payment', FALSE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTokenVersion(PaymentGatewayInterface $paymentGateway, $token_version_id) {
    // Get configuration and create client.
    $configuration = $paymentGateway->getPluginConfiguration();
    $client = $this->apiClient($configuration['user_id'], $configuration['secret']);

    // Get token version details.
    $tokenVersionService = $client->getTokenVersionService();
    return $tokenVersionService->read($configuration['space_id'], $token_version_id);
  }

}
