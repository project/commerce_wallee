<?php

namespace Drupal\commerce_wallee\Controller;

use Drupal\commerce_payment\Entity\EntityWithPaymentGatewayInterface;
use Drupal\commerce_wallee\EventSubscriber\WebhookEvent;
use Drupal\commerce_wallee\Plugin\Commerce\PaymentGateway\RedirectCheckout;
use Drupal\commerce_wallee\Services\PaymentSdk;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Wallee\Sdk\Model\TokenVersion;

/**
 * Class WebhookController for Wallee.
 */
class WebhookController extends ControllerBase {

  /**
   * The payment SDK.
   *
   * @var \Drupal\commerce_wallee\Services\PaymentSdk
   */
  protected PaymentSdk $paymentSdk;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(PaymentSdk $paymentSdk, EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher) {
    $this->paymentSdk = $paymentSdk;
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_wallee.payment_sdk'),
      $container->get('entity_type.manager'),
      $container->get('event_dispatcher'));
  }

  /**
   * Handles the webhook request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response for Wallee.
   */
  public function content(Request $request) {
    // Get data from webhook.
    $data_post = $request->getContent();

    // Check if webhook URL is in use.
    if (!$data_post) {
      throw new AccessDeniedHttpException('No payload');
    }

    $data = json_decode($data_post, TRUE);
    if (isset($data['listenerEntityTechnicalName']) && isset($data['entityId']) && isset($data['spaceId'])) {

      $event = new WebhookEvent($data);
      $this->eventDispatcher->dispatch($event);
      if ($event->isProcessed()) {
        return new Response('OK');
      }

      // Get the relevant local entity based on the remote id.
      switch ($data['listenerEntityTechnicalName']) {
        case 'TokenVersion';
          $gateway_candidates = $this->entityTypeManager()
            ->getStorage('commerce_payment_gateway')
            ->loadByProperties([
              'configuration.space_id' => $data['spaceId'],
              'plugin' => 'wallee_redirect_checkout',
            ]);

          // It is possible to have multiple gateways with the same space id,
          // that should not be a problem as this is just needed to get the
          // token.
          /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $gateway */
          $gateway = reset($gateway_candidates);
          if (empty($gateway)) {
            throw new AccessDeniedHttpException('Gateway mismatch.');
          }

          $version = $this->paymentSdk->getTokenVersion($gateway, $data['entityId']);

          if (!$version instanceof TokenVersion) {
            throw new AccessDeniedHttpException('No active version.');
          }

          if ($version->getState() == 'OBSOLETE') {
            break;
          }

          $entity = $this->loadLocalEntity($data['listenerEntityTechnicalName'], $version->getToken()->getId());
          $this->paymentSdk->webhookTokenVersion($entity->getPaymentGateway(), $data, $version);
          break;

        case 'Token':
          $entity = $this->loadLocalEntity($data['listenerEntityTechnicalName'], $data['entityId']);
          $this->paymentSdk->webhookToken($entity->getPaymentGateway(), $data);
          break;

        case 'Transaction':
          sleep(20);
          $entity = $this->loadLocalEntity($data['listenerEntityTechnicalName'], $data['entityId']);
          $this->paymentSdk->webhookTransaction($entity->getPaymentGateway(), $data);

          break;
      }
    }
    else {
      throw new AccessDeniedHttpException('Missing parameters.');
    }

    $response = new Response();
    $response->setContent('Ok');
    return $response;
  }

  /**
   * Loads the local entity that matches the remote id.
   *
   * @param string $listener_name
   *   The listener technical name as specified in Wallee backoffice.
   * @param int $remote_id
   *   The remote id.
   *
   * @return \Drupal\commerce_payment\Entity\EntityWithPaymentGatewayInterface
   *   The local entity.
   */
  protected function loadLocalEntity(string $listener_name, int $remote_id): EntityWithPaymentGatewayInterface {
    $entity_type = ($listener_name == 'Transaction') ? 'commerce_payment' : 'commerce_payment_method';
    /** @var \Drupal\commerce_payment\Entity\EntityWithPaymentGatewayInterface[] $entity_candidates */
    $entity_candidates = $this->entityTypeManager()
      ->getStorage($entity_type)
      ->loadByProperties(['remote_id' => $remote_id]);

    foreach ($entity_candidates as $entity) {
      if ($entity->getPaymentGateway()->getPlugin() instanceof RedirectCheckout) {
        return $entity;
      }
    }

    throw new AccessDeniedHttpException('No payment methods found.');
  }

}
