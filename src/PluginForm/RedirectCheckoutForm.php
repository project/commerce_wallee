<?php

namespace Drupal\commerce_wallee\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\commerce_price\Price;

/**
 * A form redirecting to the Wallee payment endpoint.
 *
 * Dokumentation
 * https://docs.drupalcommerce.org/commerce2/developer-guide/payments/create-payment-gateway/off-site-gateways/return-from-payment-provider
 * https://github.com/wallee-payment/php-sdk
 * https://app-wallee.com/de-ch/doc.
 */
class RedirectCheckoutForm extends PaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $formState) {
    $form = parent::buildConfigurationForm($form, $formState);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();
    $profiles = $order->collectProfiles();

    /** @var \Drupal\commerce_wallee\Plugin\Commerce\PaymentGateway\RedirectCheckout $redirectCheckout */
    $redirectCheckout = $payment->getPaymentGateway()->getPlugin();
    $pluginConfiguration = $redirectCheckout->getConfiguration();

    // Payment data.
    $currency = $payment->getAmount()->getCurrencyCode();
    $amount = $payment->getAmount()->getNumber();
    $order_id = $payment->getOrderId();

    /** @var \Drupal\commerce_wallee\Services\PaymentSdkInterface $payment_sdk */
    $payment_sdk = \Drupal::service('commerce_wallee.payment_sdk');

    // Create addresses.
    $billingAddress = NULL;
    if (isset($profiles['billing'])) {
      $billingAddress = $payment_sdk->createAddress($profiles['billing'], $order);
    }

    // Create addresses.
    $shippingAddress = NULL;
    if (isset($profiles['shipping'])) {
      $shippingAddress = $payment_sdk->createAddress($profiles['shipping'], $order);
    }

    // Create URLs.
    $return_url = Url::fromRoute('commerce_payment.checkout.return',
      [
        'commerce_order' => $payment->getOrder()->id(),
        'step' => 'payment',
      ],
      [
        'absolute' => TRUE,
      ])->toString();
    $cancel_url = Url::fromRoute('commerce_payment.checkout.cancel',
      [
        'commerce_order' => $payment->getOrder()->id(),
        'step' => 'payment',
      ],
      [
        'absolute' => TRUE,
      ])->toString();

    // Set environment.
    if ($pluginConfiguration['mode'] == 'test') {
      $environment = 'PREVIEW';
    }
    else {
      $environment = 'LIVE';
    }

    // Get redirect URL.
    $transaction = $payment_sdk->createTransaction(
      $pluginConfiguration['user_id'],
      $pluginConfiguration['secret'],
      $pluginConfiguration['space_id'],
      $return_url,
      $cancel_url,
      $currency,
      $order_id,
      $amount,
      $billingAddress,
      $shippingAddress,
      $environment,
      $pluginConfiguration
    );

    // Create payment.
    $paymentStorage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payment = $paymentStorage->create([
      'payment_gateway' => $order->payment_gateway->target_id,
      'order_id' => $order_id,
      'remote_id' => $transaction['transaction_id'],
    ]);
    $price = new Price($amount, $currency);
    $payment->set('state', 'pending');
    $payment->set('amount', $price);
    $payment->set('remote_state', 'pending');
    $payment->save();

    return $this->buildRedirectForm(
      $form,
      $formState,
      $transaction['redirect_url'],
      [],
      self::REDIRECT_POST
    );
  }

}
