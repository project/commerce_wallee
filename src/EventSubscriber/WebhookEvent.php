<?php


namespace Drupal\commerce_wallee\EventSubscriber;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is triggered in case of an incoming Wallee webhook.
 */
class WebhookEvent extends Event {

  /**
   * The Webhook data.
   *
   * @var array
   */
  protected array $data;

  /**
   * Flag to prevent further processing.
   *
   * @var bool
   */
  protected bool $isProcessed = FALSE;

  /**
   * Construct a new WebhookEvent class.
   *
   * @param array $data
   *   The webhook data.
   */
  public function __construct(array $data) {
    $this->data = $data;
  }

  /**
   * The webhook data.
   *
   * @return array
   */
  public function getData(): array {
    return $this->data;
  }

  /**
   * Whether the webhook should be further processed.
   * @return bool
   */
  public function isProcessed(): bool {
    return $this->isProcessed;
  }

  /**
   * @param bool $isProcessed
   */
  public function setIsProcessed(bool $isProcessed): void {
    if ($isProcessed) {
      $this->stopPropagation();
    }
    $this->isProcessed = $isProcessed;
  }

}
